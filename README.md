# TP Node 
Groupe : BOVOLENTA Alessio, BRUN-GIGLIO Alexandre, CANDOTTI Enzo, Godard Thomas.

## Setup Project 
- Pull project or download zip. 
- run `npm install`.

## Set Videos
- Create directory `assets` on `/`.
- Move video files into `assets/`.
- Rename "8K_VIDEO_ULTRAHD_120FPS" to `8k.webm`.
- Rename "720P_VIDEO_ULTRAHD_120FPS" to `720p.mp4`.

## Run Project 
- Run `npm start`.
- Open browser and go to `http://localhost:3000`.
