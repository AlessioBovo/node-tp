const express = require('express')
const fs = require('fs')
const path = require('path')
const app = express()

app.use(express.static(path.join(__dirname, 'public')))

// On définit la route qui retourne la page HTML
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + 'public/index.html'))
})

// On définit la route pour la 8K
app.get('/video8k', function (req, res) {
    const path = 'assets/8k.webm'
    const stat = fs.statSync(path)
    const fileSize = stat.size
    const range = req.headers.range

    setVideoParams(range, fileSize, path, res);
})

// On définit la route pour la 720P
app.get('/video720', function (req, res) {
    const path = 'assets/720p.mp4'
    const stat = fs.statSync(path)
    const fileSize = stat.size
    const range = req.headers.range

    setVideoParams(range, fileSize, path, res);
})

// Mise en commun de la fonction pour les parametres videos
function setVideoParams(range, fileSize, path, res) {
    if (range) {
        const parts = range.replace(/bytes=/, "").split("-")
        const start = parseInt(parts[0], 10)
        const end = parts[1] ?
            parseInt(parts[1], 10) :
            fileSize - 1

        if (start >= fileSize) {
            return
        }

        const chunksize = (end - start) + 1
        const file = fs.createReadStream(path, { start, end })
        const head = {
            'Content-Range': `bytes ${start}-${end}/${fileSize}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': chunksize,
            'Content-Type': 'video/mp4',
        }

        res.writeHead(206, head)
        file.pipe(res)
    } else {
        const head = {
            'Content-Length': fileSize,
            'Content-Type': 'video/mp4',
        }
        res.writeHead(200, head)
        fs.createReadStream(path).pipe(res)
    }
}

// On définie le port d'écoute du serveur
app.listen(3000, function () {
    console.log('Listening on port 3000!')
})