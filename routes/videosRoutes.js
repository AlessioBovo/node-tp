const videosControllers = require('../controllers/videosControllers');

const routes = (app) => {
    app.route('/')
    .get((req, res) => {
            console.log('homePage')
        }, videosControllers().getHome);

    app.route('/video720p')
        .get((req, res) => {
            console.log('video720Page')
        }, videosControllers().getVideo720p);

    app.route('/video8k')
        .get((req, res) => {
            console.log('video8KPage')
        }, videosControllers().getVideo8k);
}

export default routes;